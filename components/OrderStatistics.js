import React, { useState, useEffect } from "react";
import { format } from "date-fns";
import BarChart from "./BarChart";
import PieChart from "./Piechart";

const OrderStatistics = ({ orders }) => {
  const [barChartData, setBarChartData] = useState({
    data: [],
    labels: [],
    selectedCategory: "total", // 'total', 'paid', 'unpaid'
  });

  const [pieChartData, setPieChartData] = useState({
    data: [],
  });

  useEffect(() => {
    updateChartData();
  }, [orders, barChartData.selectedCategory]);

  const groupByDate = (array, key) => {
    return array.reduce((result, currentValue) => {
      const date = format(new Date(currentValue[key]), "yyyy-MM-dd");
      (result[date] = result[date] || []).push(currentValue);
      return result;
    }, {});
  };

  const categorizeOrders = (date, groupedOrders) => {
    const paid = groupedOrders[date].filter((order) => order.paid).length;
    const unpaid = groupedOrders[date].filter((order) => !order.paid).length;
    return { date, paid, unpaid };
  };

  const updateChartData = () => {
    const groupedOrders = groupByDate(orders, "createdAt");
    const orderedDates = Object.keys(groupedOrders);
  
    const newLabels = orderedDates.map((date) =>
      format(new Date(date), "MM/dd")
    );
  
    const sortedLabels = newLabels
      .slice()
      .sort((a, b) => new Date(a) - new Date(b));
  
    const categorizedOrders = orderedDates
      .sort((a, b) => new Date(a) - new Date(b))
      .map((date) => categorizeOrders(date, groupedOrders));
  
    const maxOrdersInADay = Math.max(
      ...categorizedOrders.map((order) => order.paid + order.unpaid)
    );
    const yAxisMax = Math.ceil(maxOrdersInADay / 10) * 10;
  
    setBarChartData({
      data:
        barChartData.selectedCategory === "total"
          ? categorizedOrders.map((order) => order.paid + order.unpaid)
          : barChartData.selectedCategory === "paid"
          ? categorizedOrders.map((order) => order.paid)
          : categorizedOrders.map((order) => order.unpaid),
      labels: sortedLabels,
      selectedCategory: barChartData.selectedCategory,
      yAxisMax,
    });
  
    setPieChartData({
      data: [
        { label: "Paid", value: categorizedOrders.reduce((acc, order) => acc + order.paid, 0) },
        { label: "Unpaid", value: categorizedOrders.reduce((acc, order) => acc + order.unpaid, 0) },
      ],
    });
  };
  


  return (
    <div>
      <h2 className="text-2xl font-bold mb-4">Order Statistics</h2>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div>
          <BarChart
            data={barChartData.data}
            labels={barChartData.labels}
            onToggle={(category) =>
              setBarChartData({ ...barChartData, selectedCategory: category })
            }
            selectedCategory={barChartData.selectedCategory}
          />
        </div>
        <div>
          <PieChart data={pieChartData.data} />
        </div>
      </div>
    </div>
  );
};

export default OrderStatistics;
