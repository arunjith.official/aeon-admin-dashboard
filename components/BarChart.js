// BarChart.js
import React, { useState, useEffect } from "react";

const BarChart = ({ data, labels, onToggle, selectedCategory }) => {
  const [maxValue, setMaxValue] = useState(0);

  useEffect(() => {
    const max = Math.max(...data);
    setMaxValue(Math.ceil(max / 10) * 10);
  }, [data]);

  const chartHeight = 300;
  const chartWidth = 500;
  const barWidth = 30;
  const barGap = 20;
  const axisMargin = 20;

  const defaultColor = "blue";

  return (
    <div className="flex justify-center">
      <svg
        height={chartHeight + axisMargin * 2}
        width={chartWidth + axisMargin * 2}
      >
        <line
          x1={axisMargin}
          y1={chartHeight + axisMargin}
          x2={chartWidth + axisMargin}
          y2={chartHeight + axisMargin}
          stroke="black"
        />
        <line
          x1={axisMargin}
          y1={axisMargin}
          x2={axisMargin}
          y2={chartHeight + axisMargin}
          stroke="black"
        />

        {data.map((value, index) => {
          const x =
            labels.indexOf(labels[index]) * (barWidth + barGap) +
            axisMargin +
            barGap;
          const y = chartHeight - (value / maxValue) * chartHeight + axisMargin;
          return (
            <g key={index}>
              <rect
                x={x}
                y={y}
                width={barWidth}
                height={(value / maxValue) * chartHeight}
                fill={defaultColor}
                onClick={() => onToggle(labels[index])}
                style={{ cursor: "pointer" }}
              />
              <text
                x={x + barWidth / 2}
                y={chartHeight + axisMargin + 15}
                textAnchor="middle"
                fontSize="10"
              >
                {labels[index]}
              </text>
              <text
                x={axisMargin - 5}
                y={y + (value === 0 ? 10 : 5)}
                textAnchor="end"
                fontSize="10"
              >
                {value}
              </text>
            </g>
          );
        })}
      </svg>
    </div>
  );
};

export default BarChart;
