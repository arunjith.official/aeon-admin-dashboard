/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['lh3.googleusercontent.com'],
    domains: ['storage.googleapis.com'],
  },
}

module.exports = nextConfig
