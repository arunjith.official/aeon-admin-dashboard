import Layout from "@/components/Layout";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";

export default function DeleteProductPage() {
  const [productInfo, setProductInfo] = useState();
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    if (!id) {
      return;
    }

    axios.get("/api/products?id=" + id).then((response) => {
      setProductInfo(response.data);
    }).catch(error => {
      // Handle the error, e.g., redirect to an error page
      console.error("Error fetching product:", error);
    });
  }, [id]);

  async function deleteProduct() {
    try {
      await axios.delete("/api/products?id=" + id);
      goBack();
    } catch (error) {
      // Handle the error, e.g., redirect to an error page
      console.error("Error deleting product:", error);
    }
  }

  function goBack() {
    router.push("/products");
  }

  return (
    <Layout>
      <h1 className="text-center">
        {productInfo ? (
          `Do you really want to delete product "${productInfo.title}"?`
        ) : (
          "Product not found."
        )}
      </h1>
      <div className="flex gap-2 justify-center">
        <button className="btn-red" onClick={deleteProduct}>
          Yes
        </button>
        <button className="btn-default" onClick={goBack}>
          No
        </button>
      </div>
    </Layout>
  );
}
