import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Layout from '@/components/Layout';

const SettingsPage = () => {
  const [featuredProduct, setFeaturedProduct] = useState('');
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetchProducts();
    fetchFeaturedProduct();
  }, []);

  const fetchProducts = () => {
    axios.get('/api/products')
      .then((response) => {
        setProducts(response.data);
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  };

  const fetchFeaturedProduct = async () => {
    try {
      const response = await axios.get('/api/featured');
      setFeaturedProduct(response.data.featuredProduct);
    } catch (error) {
      console.error('Error fetching featured product:', error);
    }
  };

  const handleSave = async () => {
    try {
      await axios.post('/api/featured', { productId: featuredProduct });
      console.log(`Selected product saved: ${featuredProduct}`);
    } catch (error) {
      console.error('Error saving featured product:', error);
    }
  };

  return (
    <Layout>
      <h1>Settings</h1>
      <div>
        <label htmlFor="featuredProduct">Set Featured Product</label>
        <select
          id="featuredProduct"
          className="mb-2"
          onChange={(ev) => setFeaturedProduct(ev.target.value)}
          value={featuredProduct}
        >
          <option value="" disabled>Select a product</option>
          {products.map((product) => (
            <option key={product._id} value={product._id}>
              {product.title}
            </option>
          ))}
        </select>
      </div>
      <button onClick={handleSave} className="btn-primary">
        Save Featured Product
      </button>
    </Layout>
  );
};

export default SettingsPage;
