import multiparty from 'multiparty';
import { Storage } from '@google-cloud/storage';
import fs from 'fs/promises';
import { mongooseConnect } from '@/lib/mongoose';
import { isAdminRequest } from './auth/[...nextauth]';

require('dotenv').config();

export default async function handle(req, res) {
  try {
    await mongooseConnect();
    await isAdminRequest(req, res);

    const form = new multiparty.Form();
    const { fields, files } = await new Promise((resolve, reject) => {
      form.parse(req, (err, fields, files) => {
        if (err) reject(err);
        resolve({ fields, files });
      });
    });

    const storage = new Storage({
      projectId: process.env.GOOGLE_CLOUD_PROJECT_ID,
      credentials: {
        type: process.env.GOOGLE_CLOUD_TYPE,
        project_id: process.env.GOOGLE_CLOUD_PROJECT,
        private_key_id: process.env.GOOGLE_CLOUD_PRIVATE_KEY_ID,
        client_id: process.env.GOOGLE_CLOUD_CLIENT_ID,
        auth_uri: process.env.GOOGLE_CLOUD_AUTH_URI,
        token_uri: process.env.GOOGLE_CLOUD_TOKEN_URI,
        auth_provider_x509_cert_url: process.env.GOOGLE_CLOUD_AUTH_PROVIDER_X509_CERT_URL,
        client_x509_cert_url: process.env.GOOGLE_CLOUD_CLIENT_X509_CERT_URL,
        universe_domain: process.env.GOOGLE_CLOUD_UNIVERSE_DOMAIN,
        client_email: process.env.GOOGLE_CLOUD_CLIENT_EMAIL,
        private_key: process.env.GOOGLE_CLOUD_PRIVATE_KEY
      }
    
    });
    const bucketName = process.env.GOOGLE_CLOUD_STORAGE_BUCKET;

    const bucket = storage.bucket(bucketName);

    const links = [];
    for (const file of files.file) {
      const ext = file.originalFilename.split('.').pop();
      const newFilename = Date.now() + '-' + '.' + ext;
      const filePath = file.path;

      await uploadFile(bucket, filePath, newFilename);
      const publicLink = `https://storage.googleapis.com/${process.env.GOOGLE_CLOUD_STORAGE_BUCKET}/${newFilename}`;
      links.push(publicLink);
    }

    res.json({ links });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

const uploadFile = async (bucket, filePath, newFilename) => {
  try {
    const fileContent = await fs.readFile(filePath);
    const file = bucket.file(newFilename);

    await file.save(fileContent, {
      metadata: {
        contentType: file.mimeType,
      },
      public: true,
    });
  } catch (error) {
    console.error('Error uploading file:', error);
    throw error;
  }
};

export const config = {
  api: { bodyParser: false },
};
