import { mongooseConnect } from "@/lib/mongoose"
import { Category } from "@/models/Category"
import { isAdminRequest } from "./auth/[...nextauth]";

export default async function handle(req, res) {
    const { method } = req
    await mongooseConnect();
    await isAdminRequest(req,res)
     
    
    if (method === 'GET') {
        res.json(await Category.find().populate("parent"));
    }

    if (method === 'POST') {
        const { name, parentCategory, properties } = req.body
        const catogoryDoc = await Category.create({
            name,
            parent: parentCategory || undefined,
            properties,
        })
        res.json(catogoryDoc)
    }
    if (method === 'PUT') {
        const { name, parentCategory, properties, _id } = req.body
        const catogoryDoc = await Category.updateOne({ _id }, {
            name,
            parent: parentCategory || undefined,
            properties
        })
        res.json(catogoryDoc)
    }
    if (method === 'DELETE') {
        const category = req.body
        await Category.deleteOne({ _id: category._id })
        res.json(true)
    }
}