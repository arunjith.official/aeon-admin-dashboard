import Layout from "@/components/Layout";
import axios from "axios";
import { useEffect, useState } from "react";

export default function OrdersPage() {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        axios.get('/api/orders')
            .then(response => {
                setOrders(response.data);  
            })
            .catch(error => {
                console.error('Error fetching orders:', error);
            });
    }, []);

    return (
        <Layout>
            <h1>Orders</h1>
            <table className="basic">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>paid</th>
                        <th>Recipient</th>
                        <th>Products</th>
                    </tr>
                </thead>
                <tbody>
                    {orders.length > 0 && orders.map(order => (
                        <tr key={order._id}>
                            <td>{new Date(order.createdAt).toLocaleString()}</td>
                            <td className={order.paid ? 'text-green-600' : 'text-red-600'}>
                                <div>{order.paid? 'YES' : 'NO'}</div>
                            </td>
                            <td>
                                <div>
                                    {order.name} {order.email}
                                </div>
                                <div>
                                    {order.city} {order.pincode}
                                </div>
                                <div>
                                    {order.streetAddress}
                                </div>
                            </td>
                            <td>
                                {order.line_items.map((item, index) => (
                                    <div key={index}>
                                        {item.price_data?.product_data.name} x {item.quantity}
                                    </div>
                                ))}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </Layout>
    );
}
