import Layout from "@/components/Layout";
import { useEffect, useState } from "react";
import axios from "axios";
import { withSwal } from 'react-sweetalert2';

function Categories({ swal }) {
  const [editedCategory, setEditedCategory] = useState(null)
  const [name, setName] = useState("");
  const [parentCategory, setParentCategory] = useState(0);
  const [categories, setCategories] = useState([]);
  const [properties, setProperties] = useState([])

  useEffect(() => {
    fetchCategories();
  }, []);

  function fetchCategories() {
    axios.get('/api/categories').then((result) => {
      setCategories(result.data);
    });
  }

  async function saveCategory(ev) {
    ev.preventDefault();
    const data = {
      name,
      parentCategory,
      properties: properties.map((p) => ({
        name: p.name,
        values: p.values.split(',')
      }))
    }
    if (editedCategory) {
      data._id = editedCategory._id
      await axios.put('/api/categories', data)
      setEditedCategory(null)
    }
    else {
      await axios.post('/api/categories', data);
    }
    setName('');
    setParentCategory('')
    setProperties([])
    fetchCategories();
  }

  function editCategory(category) {
    setEditedCategory(category)
    setName(category.name)
    setParentCategory(category.parent?._id)
    setProperties(
      category.properties.map(({ name, values }) => ({
        name,
        values: values.join(',')
      }))
    )
  }

  async function deleteCategory(category) {
    swal.fire({
      title: 'Are you sure?',
      text: `Do you want to delete ${category.name}`,
      showCancelButton: true,
      cancelButtonText: 'cancel',
      confirmButtonText: 'Yes, Delete!',
      confirmButtonColor: '#d55',
      reverseButtons: true,
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("api/categories", { data: category })
      }
      fetchCategories();
    }).catch(error => {
      
    });
  }

  function addProperty() {
    setProperties(prev => {
      return [...prev, { name: '', values: '' }]
    })
  }

  function handlePropertyNameChange(index, property, newName) {
    setProperties(prev => {
      const properties = [...prev];
      properties[index].name = newName;
      return properties
    })
  }

  function handlePropertyValueChange(index, property, newValues) {
    setProperties(prev => {
      const properties = [...prev];
      properties[index].values = newValues;
      return properties
    })
  }

  function removeProperties(indexToRemove) {
    setProperties(prev => {
      return [...prev].filter((p, index) => {
        return index !== indexToRemove
      })
    })
  }
  return (
    <Layout>
      <h1>Categories</h1>
      <label>{editedCategory ? `Edit category ${editedCategory.name}` : 'Create new category'}</label>
      <form onSubmit={saveCategory} >
        <div className="flex gap-1">
          <input
            className="mb-0"
            type="text"
            placeholder="Category name"
            onChange={(ev) => setName(ev.target.value)}
            value={name}
          />
          <select
            className="mb-0"
            onChange={(ev) => setParentCategory(ev.target.value)}
            value={parentCategory}
          >
            <option value={0}>No parent category</option>
            {categories.length > 0 &&
              categories.map((category) => (
                <option key={category._id} value={category._id}>
                  {category.name}
                </option>
              ))}
          </select>
        </div>
        <div className="mb-2 ">
          <label className="block">Properties</label>
          <button
            onClick={addProperty}
            type="button"
            className="btn-default text-sm mb-2"
          >
            Add new property
          </button>
          {properties.length > 0 &&
            properties.map((property, index) => (
              <div key={index} className="flex gap-1 mb-2">
                <input
                  type="text"
                  value={property.name}
                  onChange={(ev) => handlePropertyNameChange(index, property, ev.target.value)}
                  placeholder="property name ( example : color )"
                  className="mb-0" />
                <input
                  type="text"
                  onChange={(ev) => handlePropertyValueChange(index, property, ev.target.value)}
                  value={property.values}
                  placeholder="values"
                  className="mb-0" />
                <button
                  className="btn-red"
                  onClick={() => removeProperties(index)}
                  type="button"
                >Remove
                </button>
              </div>
            ))}
        </div>
        {editedCategory && (
          <button
            className="btn-default mr-3"
            onClick={() => {
              setEditedCategory(null);
              setName("")
               setParentCategory("")
              setProperties([])
            }}
            type="button">
            Cancel
          </button>
        )}
        <button type="submit" className="btn-primary py-1">
          Save
        </button>
      </form>
      {!editedCategory && (
        <table className="basic mt-4">
          <thead>
            <tr>
              <td>Category name</td>
              <td>Parent category</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {categories.length > 0 &&
              categories.map((category) => (
                <tr key={category._id}>
                  <td>{category.name}</td>
                  <td>{category?.parent?.name}</td>
                  <td>
                    <div className="">
                      <button
                        className="btn-default mr-1"
                        onClick={() => { editCategory(category) }}
                      >
                        Edit
                      </button>
                      <button
                        className="btn-red"
                        onClick={() => deleteCategory(category)}
                      >Delete</button>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      )}

    </Layout>
  );
}


const CategoriesWithSwal = withSwal(({ swal }, ref) => (
  <Categories swal={swal} ref={ref} />
));

export default CategoriesWithSwal;
