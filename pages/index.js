import Layout from "@/components/Layout";
import OrderStatistics from "@/components/OrderStatistics";
import axios from "axios";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";

export default function Home() {
  const { data: session } = useSession();
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    axios.get("/api/orders")
      .then((response) => {
        console.log(response.data)
        setOrders(response.data);
      })
      .catch((error) => {
        console.error("Error fetching orders:", error);
      });
  }, []);
  
  
  return (
    <Layout>
      <div className="flex items-center">
        <img src={session?.user?.image} alt="" className="w-6 h-6 mr-1 rounded-md" />
        <span className="text-sm">{session?.user?.name}</span>
        <div className="flex-grow"></div>
        <img src={session?.user?.image} alt="" className="w-6 h-6 ml-1 rounded-md" />
      </div>
      <div>
        <OrderStatistics orders={orders}/>
      </div>
    </Layout>
  );
}
